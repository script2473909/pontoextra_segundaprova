#!/bin/bash

if [ -e /tmp/$1 ]; then
	echo "existe em $(realpath /tmp/$1)"

elif [ -e /home/ifpb/$1 ]; then
	echo "o arquivo existe em $(realpath /home/ifpb/$1)"


elif [ -e /proc/$1 ]; then
	echo "o arquivo existe em $(realpath /proc/$1)"


elif [ -e /var/log/$1 ]; then
	echo "o arquivo existe em $(realpath /var/log/$1)"

else 
	echo "Não existe em nenhuma pasta"	

fi
