#!/bin/bash

linhas1=$(wc -l < "$1" 2>/dev/null)
linhas2=$(wc -l < "$2" 2>/dev/null)
linhas3=$(wc -l < "$3" 2>/dev/null)
linhas4=$(wc -l < "$4" 2>/dev/null)

max="$linhas1"
file="$1"

if [ $linhas2 -ge $linhas1 ]; then
	max="$linhas2"
	file="$2"

fi

if [ $linhas3 -ge $linhas2 ]; then
	max="$linhas3"
	file="$3"
fi

if [ $linhas4 -ge $linhas3 ]; then
	max="$linhas4"
	file="$4"
fi

#echo o maior arquivo é o "$file" com $max linhas
cat $file
